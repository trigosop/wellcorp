'use strict';

var message = '';
var messageBox = document.getElementById('modal-message');

var totalPorcentajeEmpleados = 100;
var inputHombres = document.getElementById('empleados_hombres');
var inputMujeres = document.getElementById('empleados_mujeres');

$(document).ready(function () {
  handleProgramType();
  handleEmpleadosPorSexo();
  $('.select2js').select2({
    minimumResultsForSearch: Infinity,
    selectionCssClass: ':all:',
    dropdownCssClass: ':all:',
  });
});

function googleCheck(res, form) {
  var response = document.querySelector(`#${form} .g-recaptcha-response`).value;

  if (res == '' || response == '') {
    // Actualiza modal con nuevo mensaje
    messageBox.innerHTML = 'Por favor completa el captcha para continuar.';

    // Mostrar modal
    $('.modal').modal('show');

    return false;
  }

  return true;
}

function handleEmpleadosPorSexo() {
  inputHombres.addEventListener('input', function (e) {
    inputMujeres.value = 100 - parseInt(e.target.value);
  });
  inputMujeres.addEventListener('input', function (e) {
    inputHombres.value = 100 - parseInt(e.target.value);
  });
}

function handleProgramType() {
  var opciones = document.querySelectorAll('[name=programType]');
  var sumateEmpoyes = document.querySelector('#sumate-employes');
  var sumateEmail = document.querySelector('#sumate-email');
  var sumatePhone = document.querySelector('#sumate-phone');

  Array.prototype.forEach.call(opciones, function (element, index) {
    element.addEventListener('click', function (element) {
      if (element.target.id === 'distribute') {
        sumateEmpoyes.style.display = 'none';
        sumateEmpoyes.setAttribute('required', false);
        sumateEmail.classList.add('half');
        sumatePhone.classList.add('half');
      } else {
        sumateEmpoyes.style.display = 'block';
        sumateEmpoyes.setAttribute('required', true);
        sumateEmail.classList.remove('half');
        sumatePhone.classList.remove('half');
      }
    });
  });
}

function handleSubmit(event) {
  event.preventDefault();

  var form = event.target.id;

  if (googleCheck(null, form)) {
    var formData = new FormData(event.target); // Variable con la info de formulario
    // DEBUG ONLY
    for (var pair of formData.entries()) {
      console.log(pair[0] + ', ' + pair[1]);
    }
    // Enviar consulta a API
    //
    //
    // ------

    // Actualiza modal con nuevo mensaje
    // - Error
    messageBox.innerHTML = 'Ocurrió un error, intente nuevamente más tarde.';

    // - Confirmación
    messageBox.innerHTML =
      'Gracias por dejarnos tus datos de contacto en breve nos estaremos poniendo en contacto con vos.';

    // ------

    // Mostrar modal
    $('.modal').modal('show');
  }

  return false;
}
